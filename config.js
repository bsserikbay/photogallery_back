const path = require("path");

const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, "public", "uploads"),
  db: {
    name: "exam_12",
    url: "mongodb://localhost"
  },
  facebook: {
    appId: "3343282342401995",
    secret: "cd67d8c7033f44078d95e180999a5b99"
  }
};
