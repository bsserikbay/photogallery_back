const mongoose = require("mongoose");
const config = require("./config");
const User = require("./models/User");
const Photo = require("./models/Photo");

mongoose.connect(`${config.db.url}/${config.db.name}`, {
    useNewUrlParser: true, useFindAndModify: false, useUnifiedTopology: true, w: "majority", family: 4
});

const db = mongoose.connection;

db.once("open", async () => {
    try {
        await db.dropCollection("photos");
        await db.dropCollection("users");
    } catch (e) {
        console.log("Collection were not presented. Skipping drop...");
    }

    const [user, admin] = await User.create({
        username: "user",
        password: "user",
        role: "user"
    }, {
        username: "admin",
        password: "admin",
        role: "admin"
    });


    await Photo.create(
        {
            title: "Yay, this is my cool CPU!",
            image: "cpu.jpg",
            user: user._id
        },
        {
            title: "My Cat",
            image: "cat.jpg",
            user: user._id
        },
        {
            title: "Yay, this is my cool CPU!",
            image: "cpu.jpg",
            user: admin._id
        },
        {
            title: "I bought Seagate 3TB",
            image: "hdd.png",
            user: admin._id
        },
    );


    db.close();
});
