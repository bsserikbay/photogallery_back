const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const photos = require("./app/photos");
const users = require("./app/users");
const config = require("./config");
const app = express();
const PORT = 8000;

app.use(cors());
app.use(express.static("public"));
app.use(express.json());

mongodb: mongoose.connect(`${config.db.url}/${config.db.name}`,
  {
    useNewUrlParser: true, useFindAndModify: false, useUnifiedTopology: true, w: "majority", family: 4
  }
).then(() => {
	console.log("Mongoose connected!");
	app.use("/photos", photos());
	app.use("/users", users());

	app.listen(PORT, () => {
		console.log("Server started at http://localhost:" + PORT);
	});
});





