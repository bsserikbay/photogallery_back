const express = require("express");
const multer = require("multer");
const path = require("path");
const {nanoid} = require("nanoid");
const config = require("../config");
const Photo = require("../models/Photo");
const User = require("../models/User")
const auth = require("../middleware/auth");
const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = () => {
    router.get("/", async (req, res) => {
        let user;
        if (req.query.user) {
            user = {user: req.query.user};
            try {
                const photos = await Photo.find(user).populate("user");
                res.send(photos)
            } catch (e) {
                res.sendStatus(500);
            }
        } else {
            try {
                const photos = await Photo.find().populate("user");
                res.send(photos)
            } catch (e) {
                res.sendStatus(500);
            }
        }
    });


    router.get("/:id", async (req, res) => {
        const photo = await Photo.findById(req.params.id);
        res.send(photo);
    });


    router.post("/", [upload.single("image"), auth], async (req, res) => {

        const token = req.get("Token");
        const user = await User.findOne({token})
        const userId = user._id
        const photo = new Photo(req.body);
        if (req.file) {
            photo.image = req.file.filename;
            photo.user = userId
            await photo.save();
            res.send(photo);
        } else{
            return  res.sendStatus(400);
        }

    });

    router.delete("/:id", auth, async (req, res) => {
        const token = req.get("Token");
        const user = await User.findOne({token})
        const id = req.params.id
        try{
            await Photo.deleteOne({_id:id, user})
            res.sendStatus(200)
        }catch(e) {
            res.sendStatus(e, 'something wrong here')
        }
    });

    return router;
}


module.exports = createRouter;
